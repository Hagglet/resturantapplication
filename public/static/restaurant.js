
// Get the location to the restaurant and will show it in the page on the site
var map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([14.7752, 56.878]),
      zoom: 18
    })
});

let name = document.getElementById("Head2")
let picture = document.getElementById("picture")
let information = document.getElementById("informationDiv")
let commentDiv = document.getElementById("commentDiv")
let rating_str = ""
// fetch the data from the json and insert the data to the site 
fetch('/data')
.then((resp) => resp.json())
.then((resp) => {
   name.innerText = resp.name
   picture.src = resp.image
   information.innerText = resp.description
   for (let star = 0; star < 5; star++) {
       if(star < resp.rating)
       {
        let starEl = document.getElementById("star"+star)
        starEl.setAttribute("class","fa fa-star checked")
        }          
   }
   let reviewBlock = '';
   // create a new review 
   resp.reviews.forEach(element => { 

      // start bit
       reviewBlock += `
       <div class='friend'>
           <h3 id = "reviewsName">${element.author}</h3>
           <p id = "reviewsComment">${element.review}</p> 
           `
           for (let star = 0; star < 5; star++) {
         
            if(star < element.rating)
            {
              reviewBlock += `<span id = "${element.author}star0" class="fa fa-star checked"></span>`
             }else{
              reviewBlock += `<span id = "${element.author}star0" class="fa fa-star"></span>`
             }       
           }

             // end bit
      reviewBlock += `</div>`
      document.getElementById("commentDiv").innerHTML = reviewBlock
      
    
       
   });
})

