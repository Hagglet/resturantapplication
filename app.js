const express = require('express')
const app = express()
const path = require('path')
const {PORT = 8080 } = process.env


app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

// Declare end points 
app.get('/', (req,res)=> {
    return res.sendFile(path.join(__dirname,'public', 'index.html'))
})

app.get('/data',(req, res)=>{
    console.log("data has been fethced")
     return res.sendFile(path.join(__dirname,'data.json'))
 })

app.get('/restaurant', (req,res)=> {
    return res.sendFile(path.join(__dirname,'public', 'restaurant.html'))
})

app.listen(PORT, ()=> console.log('server started on port..'))